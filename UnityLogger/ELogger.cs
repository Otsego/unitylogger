﻿using System;
using System.Diagnostics;
using Debug = UnityEngine.Debug;

namespace UnityLogger
{
    public static class ELogger
    {
        [Conditional("ENABLE_INFO_LOGS")]
        public static void Log(object message)
        {
            Debug.Log(message);
        }
        
        [Conditional("ENABLE_ERROR_LOGS")]
        public static void LogError(object message)
        {
            Debug.LogError(message);
        }
        
        [Conditional("ENABLE_WARNING_LOGS")]
        public static void LogWarning(object message)
        {
            Debug.LogWarning(message);
        }

        [Conditional("ENABLE_EXCEPTION_LOGS")]
        public static void LogException(Exception exception)
        {
            Debug.LogException(exception);
        }
    }
}